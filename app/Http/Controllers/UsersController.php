<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use App\Status;
use App\Role;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\Department;
use App\Candidate;
use App\User;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $users = User::all();
        $departments = Department::all();
        $roles=Role::all();
        return view('users.index', compact('users','departments','roles'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        $departments = Department::all();
        
        return view('users.edit',compact('user','departments'));
    }



    

    

    public function update(Request $request, $id)
    {
        $user = user::findOrFail($id);
        if(Gate::allows('delete-users', $user)){

           $user-> update($request->all());
           if(!isset($request->password )){
            $request['password'] = $user->password; 
           }else{
             $request->password = Hash::make($request['password']);   
           } 
        }
        else{
            Session::flash('notallowed', 'You are not allowed to delete/edit the user  becuase you are not the admin ');
        }

        return redirect('users');


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


   


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if(Gate::allows('delete-users', $user))
    {
        $user->delete(); 
    }else{
            Session::flash('notallowed', 'You are not allowed to delete/edit the user  becuase you are not the admin ');
        }
        return redirect('users'); 
        //
    }
}
