<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = [
        'name',
        'email',
        'user_id',
        
        'age',
          
    ];

    public function status_can (){
        return $this->belongsTo('App\Status','status_id');
    }

  

    
    public function owner (){
        return $this->belongsTo('App\User','user_id');
    }
}

