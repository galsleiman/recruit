<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            
            ['name'=> ('before interview'),'created_at' =>date('Y-m-d G:i:s'),'updated_at' =>date('Y-m-d G:i:s')],
            ['name' => ('not fit'),'created_at' =>date('Y-m-d G:i:s'),'updated_at' =>date('Y-m-d G:i:s')],
            ['name' => ('sent to manager'),'created_at' =>date('Y-m-d G:i:s'),'updated_at' =>date('Y-m-d G:i:s')],
            ['name' => ('not fit professionally'),'created_at' =>date('Y-m-d G:i:s'),'updated_at' =>date('Y-m-d G:i:s')],
            ['name' => ('ccepted to work'),'created_at' =>date('Y-m-d G:i:s'),'updated_at' =>date('Y-m-d G:i:s')],
        ]); 
    }
}
