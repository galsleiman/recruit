<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
   
    /**
     * Seed the application's database.
     *
     * @return void 
     */
    public function run(){
        DB::table('candidates')->insert([
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'created_at' =>date('Y-m-d G:i:s'),
            'updated_at' =>date('Y-m-d G:i:s'),
          
            ]);

          
               
    
        $this->call(UserSeeder::class);
}
        
    
}
