@extends('layouts.app')

@section('content')    
@section('title', 'Create candidate')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Create candidate</h1>

        @if ($errors->any())
        <div class="alert alert-info" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        
         
                    
                    <form method = "post" action = "{{action('CandidatesController@store')}}">
                    @csrf
                    <div>

                        <label for = "name"> Candidate name </label>
                        <input type = "text" name = "name">
                    </div>   
                    
                    <div>
                        <label for = "email"> Candidate email</label>
                        <input type = "text"  name = "email">
                    </div>
                    <div>
                        <label for = "age">Candidate Age</label>
                        <input type = "number"  name = "age">
                    </div>


                    <div>
                    <button type="submit" class="btn btn-primary">Create candidate</button>
                   
                    </div>

                    </form>
                    @endsection
