@extends('layouts.app')


@section('content')  
@section('title', 'Edit user')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a user-only admin</h1>

        @if ($errors->any())
        <div class="alert alert-info" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif 
                    <form method = "post" action = "{{action('UsersController@update', $user->id)}}">
                    @method('PATCH') 
                    @csrf
                    
                    <div>
                        <label for = "name"> user name </label>
                        <input type = "text" name = "name" value = {{$user->name}} >
                    </div>   
                    
                    <div>
                        <label for = "email"> user email</label>
                        <input type = "text" name = "email" value = {{$user->email}} >
                    </div>
                    <div>
                        <label for = "department_id"> user department</label>
                        <div class="col-md-6">
                            <select class="form-control" name="department_id">                                                                         
                               @foreach ($departments as $department)
                               @if($user->department->id == $department->id)
                                 <option value="{{ $department->id }}" selected="selected"> 
                                     {{ $department->name }} 
                                 </option>
                               @else
                               <option value="{{ $department->id }}"> 
                                    {{ $department->name }} 
                                </option>
                               @endif                                      
                               @endforeach    
                             </select>
                             
                        </div>
                    </div>
                    <div>
                        <label for="password" >{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    

                    <div>
                        <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    
                    

    
                     </form>
                    </div>
                    </div>
                   
                    @endsection
 