
@extends('layouts.app')

  
@section('content')  
@section('title', 'USERS')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

              
        <h2 class="text-center"> List of users</h2>
      </br>  <table class="table table-bordered" style="width:70%;" align="center"   border-collapse="cllapse" style="width:70%;">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><<th>Departments</th><th>roles</th><th>Created</th><th>Updated</th> <th>Edit</th><th>Delet</th>   
            </tr>
            <!-- the table data --> 
            @foreach($users as $user)
        
                <tr>
                    <td>{{ $user->id}}</td>
                    <td>{{ $user->name}}</td>
                    <td>{{ $user->email}}</td>
                    <td>  {{$user->department->name}}</td>
                    <td>   <select class="form-control" name="role_id"> 
                        @foreach(App\Role::roleuser($user->id) as $role)
                        <option value="{{ $role->id}}"> 
                           {{$role->name}}
                        </option>
                        @endforeach  
                      </td>
                    
    
                    <td>{{ $user->created_at}}</td>
                    <td>{{ $user->updated_at}}</td>
                    <td>
    <a href="{{ route('users.edit',$user->id)}}" class="btn btn-primary"> Edit</a>

</td>

<td>
  <a href = "{{route('user.delete',$user->id)}}">Delete</a>
</td>  
            
                
            </tr>
        
@endforeach

        </table>       
     

        </div>

        @endsection
