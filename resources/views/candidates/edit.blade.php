@extends('layouts.app')


@section('content')  
@section('title', 'Edit candidate')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a contact</h1>

        @if ($errors->any())
        <div class="alert alert-info" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif 
                    <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
                    @method('PATCH') 
                    @csrf
                    
                    <div>
                        <label for = "name"> Candidate name </lable>
                        <input type = "text" name = "name" value = {{$candidate->name}} />
                    </div>   
                    
                    <div>
                        <label for = "email"> Candidate email</lable>
                        <input type = "text" name = "email" value = {{$candidate->email}} >
                    </div>
                    <div>
                        <label for = "age"> Candidate age</lable>
                        <input type = "namber" name = "age" value = {{$candidate->age}} >
                    </div>

                    
                     <button type="submit" class="btn btn-primary">Update</button>

    
                     </form>
                    </div>
                    </div>
                   
                    @endsection
 