@extends('layouts.app')

  
@section('content')  
@section('title', 'info')
<div class="row">
    <div class="col-sm-8 offset-sm-2">

<h2 class="text-center"> info candidat</h2>

<div>
<label for = "name"> candidate id:</label>
{{ $candidate->id}} 
</div>
<div>
<label for = "id"> candidate name: </label>
{{ $candidate->name}} 
    </div>
<div>
<label for = "email"> candidate email: </label>
{{ $candidate->email}} </div> 
<div>
<label for = "age"> candidate age :
</label>
{{ $candidate->age}} 
</div>
<div>
<label for = "status_id"> candidate status:</label>    {{$candidate->status_can->name}}
</div>
<div >
<label for = "status_id">  change candidate status:</label>
<div class="col-md-6">
    <form method="POST" action="{{ route('candidate.changestatusinf') }}">
        @csrf  
        <div class="form-group row">
        <label for="status_id" class="col-md-4 col-form-label text-md-right"></label>
        <div class="col-md-6">
            @if (null != App\Status::next($candidate->status_id)) 
            <select class="form-control" name="status_id">                                                                         
              @foreach (App\Status::next($candidate->status_id) as $status)
              <option value="{{ $status->id }}"> 
                  {{ $status->name }} 
              </option>
              @endforeach    
            </select>
        </div>
        <input name="id" type="hidden" value = {{$candidate->id}} >
        <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Change status
                    </button>
                </div>
                @else   The candidate has reached a final state, cannot be changed
                @endif

        </div>                    
    </form> 
        @endsection